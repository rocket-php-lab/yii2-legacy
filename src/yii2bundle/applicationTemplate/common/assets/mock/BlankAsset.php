<?php

namespace yii2bundle\applicationTemplate\common\assets\mock;

use yii\web\AssetBundle;

/**
 * Class BlankAsset
 *
 * Define in env config
 *
 *```php
 * 'container' => [
 *      'yii\bootstrap\BootstrapAsset' => [
 *          'class' => 'yii2bundle\applicationTemplate\common\assets\mock\BlankAsset',
 *      ],
 * ],
 * ```
 *
 * @package yii2bundle\applicationTemplate\common\assets\mock
 *
 */
class BlankAsset extends AssetBundle {

}
