<?php

return [
	'mainMenu' => [
		'yii2bundle\rest\web\helpers\Menu',
	],
	'rightMenu' => [
		'yii2bundle\account\module\helpers\Menu',
	],
];