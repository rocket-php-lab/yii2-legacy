<?php

namespace yii2bundle\rest\domain\exceptions;

use yii\web\ServerErrorHttpException;

class UnavailableRestServerHttpException extends ServerErrorHttpException {

}