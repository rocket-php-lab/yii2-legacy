<?php

namespace yii2bundle\notify\domain\interfaces\repositories;

use yii2rails\domain\interfaces\repositories\CrudInterface;

/**
 * Interface SmsQueueInterface
 * 
 * @package yii2bundle\notify\domain\interfaces\repositories
 * 
 * @property-read \yii2bundle\notify\domain\Domain $domain
 */
interface SmsQueueInterface extends CrudInterface {

}
