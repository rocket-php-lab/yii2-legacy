<?php

namespace {owner}\{name}\api\controllers;

use yii2bundle\rest\domain\rest\ActiveControllerWithQuery as Controller;

class {Entity}Controller extends Controller
{
	
	public $service = '{name}.{entity}';
	
}
