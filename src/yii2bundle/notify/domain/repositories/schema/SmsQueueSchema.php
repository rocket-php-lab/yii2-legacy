<?php

namespace yii2bundle\notify\domain\repositories\schema;

use yii2rails\domain\repositories\relations\BaseSchema;

/**
 * Class SmsQueueSchema
 * 
 * @package yii2bundle\notify\domain\repositories\schema
 * 
 */
class SmsQueueSchema extends BaseSchema {

}
