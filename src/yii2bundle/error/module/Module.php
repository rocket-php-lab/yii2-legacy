<?php

namespace yii2bundle\error\module;

use yii\base\Module as YiiModule;

/**
 * error module definition class
 */
class Module extends YiiModule
{
	static $langDir = '@vendor/rocket-php-lab/yii2-legacy/src/yii2bundle/error/domain/messages';
}
