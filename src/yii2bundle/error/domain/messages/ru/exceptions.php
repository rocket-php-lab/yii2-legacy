<?php

return [
	
	'title' => 'Ошибка',
	
	'yii2rails\\init\\domain\\exceptions\\NotInitApplicationException' => [
		//'title' => '',
		'name' => 'Приложение не инициализировано',
		'message' => 'Инициализируйте приложение',
	],

    'yii2rails\\domain\\exceptions\\UnprocessableEntityHttpException' => [
        //'title' => '',
        'name' => 'Переданы неверные данные',
    ],

];
