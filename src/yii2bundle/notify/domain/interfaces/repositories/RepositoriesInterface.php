<?php

namespace yii2bundle\notify\domain\interfaces\repositories;

/**
 * Interface RepositoriesInterface
 * 
 * @package yii2bundle\notify\domain\interfaces\repositories
 * 
 * @property-read \yii2bundle\notify\domain\interfaces\repositories\EmailInterface $email
 * @property-read \yii2bundle\notify\domain\interfaces\repositories\SmsInterface $sms
 * @property-read \yii2bundle\notify\domain\interfaces\repositories\SmsQueueInterface $smsQueue
 * @property-read \yii2bundle\notify\domain\interfaces\repositories\TestInterface $test
 */
interface RepositoriesInterface {

}
